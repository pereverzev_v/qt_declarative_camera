import QtQuick 2.12
import QtQuick.Window 2.12
import QtMultimedia 5.12

Window {
    id: root

    width: Screen.width
    height: Screen.height

    visible: true
    title: qsTr("Hello World")

    CameraView {
        id: cameraView
    }
}
