import QtQuick 2.0
import QtMultimedia 5.12


Item {
    id: root

    anchors.fill: parent

    VideoOutput {
        id: viewFinder

        anchors.fill: parent

        source: myCamera

        focus : visible

        autoOrientation: true

        fillMode: VideoOutput.PreserveAspectFit
    }

    Connections {
        target: myCamera

        onImageSaved: {
            previewImage.source = "image://preview/" + imageName
        }
    }

    Image {
        id: previewImage

        height: parent.height/4
        width: parent.width/4
    }

    Rectangle {
        anchors.bottom: parent.bottom
        anchors.horizontalCenter: parent.horizontalCenter

        width: 50
        height: 50

        color: "red"

        radius: 10

        MouseArea {
            anchors.fill: parent

            onClicked: {
                myCamera.capture()
            }
        }
    }
}
