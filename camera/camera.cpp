#include "camera.h"

#include <QCameraInfo>
#include <QCameraFocus>
#include <QImageEncoderSettings>
#include <QStandardPaths>
#include <QDebug>
#include <QDateTime>
#include "bufferedpreviewimageprovider.h"

Camera::Camera(QObject *parent) : QObject (parent)
{
    setupCamera();

    setupImageCapture();

    connect(m_imageCapture, &QCameraImageCapture::imageCaptured, [](int id, const QImage &preview) {
        qDebug() << "image captured";
    });

    connect(m_imageCapture, &QCameraImageCapture::imageSaved, [](int id, const QString &fileName) {
        int i = 0;
    });

    connect(m_imageCapture, &QCameraImageCapture::imageAvailable, [this](int id, const QVideoFrame &frame) {
        QImage image = imageFromVideoFrame(frame);

        QString imageName = QString("/%1.jpg").arg(QDateTime::currentDateTime().toString());

        BufferedPreviewImageProvider::instance()->registerImage(imageName, image);

        emit imageSaved(imageName);

        qDebug() << "image saved";
    });

    m_camera->start();
}

QObject *Camera::mediaObject() const
{
    return m_camera;
}

void Camera::capture()
{
    m_camera->searchAndLock();

    m_imageCapture->capture();

    m_camera->unlock();
}

void Camera::saveImage(const QImage &image)
{
    QString download = QStandardPaths::writableLocation(QStandardPaths::DownloadLocation);

    QString imageName = QString("/%1.jpg").arg(QDateTime::currentDateTime().toString());

    image.save(download + imageName);
}

void Camera::setupCamera()
{
    m_camera = new QCamera(QCamera::BackFace, this);

    m_camera->setCaptureMode(QCamera::CaptureStillImage);

    m_camera->focus()->setFocusMode(QCameraFocus::ContinuousFocus);
}

void Camera::setupImageCapture()
{
    m_imageCapture = new QCameraImageCapture(m_camera);

    m_imageCapture->setCaptureDestination(QCameraImageCapture::CaptureToBuffer);

    QImageEncoderSettings encodingSettings = m_imageCapture->encodingSettings();

    encodingSettings.setQuality(QMultimedia::VeryHighQuality);

    m_imageCapture->setEncodingSettings(encodingSettings);

    m_imageCapture->setBufferFormat(QVideoFrame::Format_Jpeg);
}

QImage Camera::imageFromVideoFrame(const QVideoFrame& buffer) const
{
    QImage img;

    QVideoFrame frame(buffer);

    frame.map(QAbstractVideoBuffer::ReadOnly);

    QImage::Format imageFormat = QVideoFrame::imageFormatFromPixelFormat(frame.pixelFormat());

    if (imageFormat != QImage::Format_Invalid) {
        img = QImage(frame.bits(),
                     frame.width(),
                     frame.height(),
                     imageFormat);
    } else {
        // e.g. JPEG
        int nbytes = frame.mappedBytes();
        img = QImage::fromData(frame.bits(), nbytes);
    }

    frame.unmap();

    return img;
}
