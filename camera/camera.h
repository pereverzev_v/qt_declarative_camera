#ifndef CAMERA_H
#define CAMERA_H


#include <QCamera>
#include <QObject>
#include <QCameraImageCapture>

class Camera : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QObject* mediaObject READ mediaObject NOTIFY mediaObjectChanged)

public:
    explicit Camera(QObject *parent = nullptr);

    QObject* mediaObject() const;

    Q_INVOKABLE void capture();

    Q_INVOKABLE void saveImage(const QImage &image);

signals:
    void mediaObjectChanged(QObject* mediaObject);

    void imageSaved(const QString &imageName);

private:
    void setupCamera();

    void setupImageCapture();

    QImage imageFromVideoFrame(const QVideoFrame &frame) const;

    QCamera *m_camera;
    QCameraImageCapture *m_imageCapture;
};

#endif // CAMERA_H
