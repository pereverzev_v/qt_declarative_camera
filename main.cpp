#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include "camera/camera.h"
#include <QApplication>
#include "bufferedpreviewimageprovider.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QApplication app(argc, argv);

    QQmlApplicationEngine engine;
    const QUrl url(QStringLiteral("qrc:/main.qml"));

    QQmlContext *ctx = engine.rootContext();

    ctx->setContextProperty("myCamera", new Camera());

    engine.addImageProvider(QString("preview"), BufferedPreviewImageProvider::instance());

    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
