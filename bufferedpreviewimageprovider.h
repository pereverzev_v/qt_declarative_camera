#ifndef BUFFEREDPREVIEWIMAGEPROVIDER_H
#define BUFFEREDPREVIEWIMAGEPROVIDER_H


#include <QQuickImageProvider>
#include <QHash>

class BufferedPreviewImageProvider : public QQuickImageProvider
{
public:

    static BufferedPreviewImageProvider *instance() {
        static BufferedPreviewImageProvider s;
        return &s;
    }

public:
    QImage requestImage(const QString &id, QSize *size, const QSize &requestedSize);

    void registerImage(const QString &id, const QImage &image);

private:
    BufferedPreviewImageProvider();

    QHash<QString, QImage> m_imageHash;
};

#endif // BUFFEREDPREVIEWIMAGEPROVIDER_H
