#include "bufferedpreviewimageprovider.h"

BufferedPreviewImageProvider::BufferedPreviewImageProvider() : QQuickImageProvider (QQuickImageProvider::Image)
{

}

QImage BufferedPreviewImageProvider::requestImage(const QString &id, QSize *size, const QSize &requestedSize)
{
    return m_imageHash.value(id);
}

void BufferedPreviewImageProvider::registerImage(const QString &id, const QImage &image)
{
    m_imageHash.insert(id, image);
}
